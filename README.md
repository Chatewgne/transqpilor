# TransQpilor

This project is a transcompiler, translating SeQreL to SQL.

## Compilation

```make```

## Usage

``` ./bin/compiler < ./input/input_file```