%{
  #include <math.h>
	#include <stdio.h>
	#include <string.h>
  #include "../header/instruction.h"
  #include "../header/ast.h"
  #include "../header/stack.h"

  int yylex(void);
	void yyerror(char*);

  char * suffix_buffer;
  char * pred_buffer;
  char * query_buffer;
  struct Stack* stack;

	int yydebug = 1;
	extern int yylineno;

	int constant = 0;
	char* type;	

%}

%union {
	char* str;
	int nb;
}

%token tSELECT tFROM tWHERE tSEP tTAGO1 tTAGC1 tSELECTALL tORDERBY tASC tDESC tUNION tGROUPBY tDOT
%token tALL_FROM tTAGO3 tTAGC3 tTAGO4 tTAGC4 tTAGO5 tTAGC5 tTAGO6 tTAGC6 tEQ tDIFF tSUPEQ tINFEQ tSUP tINF tSEP2 tAND tOR
%token tIN tNOTIN tSUBIN tSUBNOTIN tAS tASREF tDISTINCT tSUM tAVG tMAX tMIN tCOUNT tAP
%token <str> tID tNB
%type <str> comp_op column_ref value column_location renamed_set_function set_quantifier column_name sort_key ordering_spec sort_spec table_ref set_function_type set_function


%%

start: {stack = createStack();} select_or_order_query ;

select_or_order_query: select_query 
                     | tORDERBY { suffix_buffer = new_buffer(); 
                                  concat_buff("ORDER BY", suffix_buffer, SPACE);
                                }
                     ordered_select_query tTAGC1 {add_to_query(pop(stack),SPACE);};

ordered_select_query: sort_list { push(stack,suffix_buffer); }  tSEP select_query;

sort_list: tTAGO2 sort_spec {          const char * str = ", ";
                                     strncat($2,str,2);
                                     concat_buff($2,suffix_buffer, NOSPACE);
                          } tSEP sort_list tSUP
          | tTAGO1 sort_spec { concat_buff($2,suffix_buffer, NOSPACE);} tTAGC1 ;

sort_spec: sort_key ordering_spec {
                                    char c=' ';
                                    strncat($1,&c,1);
                                    strcat($1,$2);
                                    $$=$1;
                                  };

ordering_spec: tASC  {$$="ASC";} 
             | tDESC {$$="DESC";} ;

sort_key: column_name {$$=$1;} ;

select_query: simple_table 
              | tUNION {
                        suffix_buffer = new_buffer();
                        concat_buff("UNION", suffix_buffer, NOSPACE);
                        push(stack,suffix_buffer);
                       } select_query tSEP2
                       {
                         add_to_query(pop(stack),SPACE);
                       }
                        select_query tTAGC5
              | tGROUPBY { suffix_buffer = new_buffer(); 
                           concat_buff("GROUP BY", suffix_buffer, SPACE);}
                grouped_simple_table tTAGC6 {add_to_query(pop(stack),SPACE);} ;

grouped_simple_table: grouping_column_list { push(stack,suffix_buffer); } tSEP simple_table;

grouping_column_list: tTAGO2 column_name {          
                                     const char * str = ", ";
                                     strncat($2,str,2);
                                     concat_buff($2,suffix_buffer, NOSPACE);
                                      } tSEP grouping_column_list tTAGC2
          | tTAGO1 column_name { concat_buff($2,suffix_buffer, NOSPACE);} tTAGC1 ;


simple_table: query_all_from 
            | query_list
            | query_simple 
            ;

query_all_from: tSELECTALL {add_to_query("SELECT *", SPACE);} table_expr ;

query_simple: tSELECT {add_to_query("SELECT", SPACE);} tTAGO1 column_ref {add_to_query($4, SPACE);} tTAGC1 table_expr ;

query_list:  tSELECT {add_to_query("SELECT", SPACE);} select_list  table_expr ;

table_expr: all_from_table_expr 
          | from_where_table_expr
          ;
from_where_table_expr: from_clause where_clause ;

all_from_table_expr: tALL_FROM {add_to_query("FROM", SPACE);} table_ref_list ;  

from_clause: tFROM {add_to_query("FROM", SPACE);} table_ref_list;

table_ref_list: tTAGO1 table_ref { add_to_query($2,SPACE); } tTAGC1
           | tTAGO2 table_ref_in_group tSEP table_ref_list tTAGC2 ;

where_clause: tWHERE {add_to_query("WHERE", SPACE); } search_cond { add_to_query(tree_to_string(),SPACE); free_tree(); } ; 

search_cond: and_clause
            | or_clause
            | predicate 
            ;

and_clause: tAND {add_node("AND");} search_conds tTAGC1 ;

or_clause: tOR {add_node("OR");} search_conds tTAGC1 ;

search_conds: search_cond tSEP search_cond ;

comp_predicate: tTAGO3 predicate_id tTAGC3 
         | tTAGO4 predicate_nb tTAGC4
         ;  

predicate: comp_predicate
         | in_predicate 
         | not_in_predicate ;

in_predicate: tIN column_location tSEP {
                                        char * in = " IN (\0";
                                        pred_buffer=new_buffer();
                                        strncat($2,in,5);
                                        concat_buff($2,pred_buffer, NOSPACE);
                                       }
                                        value_list tTAGC1 { 
                                                            char * par = ")\0";
                                                            concat_buff(par,pred_buffer,NOSPACE);
                                                            add_leaf(pred_buffer);};

not_in_predicate: tNOTIN column_location tSEP {
                                        char * in = " NOT IN (\0";
                                        pred_buffer=new_buffer();
                                        strncat($2,in,9);
                                        concat_buff($2,pred_buffer, NOSPACE);
                                       }
                                        value_list tTAGC1 { 
                                                            char * par = ")\0";
                                                            concat_buff(par,pred_buffer,NOSPACE);
                                                            add_leaf(pred_buffer);};

value: tAP tID tAP {
                    char * ap = "'\0";
                    $$=concat3(ap,$2,ap,NOSPACE);
                   } ;

value_list: tTAGO1 value tTAGC1  { concat_buff($2,pred_buffer, NOSPACE);}
          | tTAGO2 value {  
                            const char * str = ", ";
                            strncat($2,str,2);
                            concat_buff($2,pred_buffer, NOSPACE);
                          } tSEP value_list tTAGC2 ;

predicate_id: column_name comp_op tID {
                                // ID can be either column name or string value  
                                add_leaf(concat3($1,$2,$3,SPACE));
                                //add_3_to_query($1,$2,$3,SPACE);  
                              } ;

predicate_nb: tID comp_op tNB { 
                                add_leaf(concat3($1,$2,$3,SPACE));
                                //add_3_to_query($1,$2,$3,SPACE);  
                              } ;

comp_op: tEQ {$$ = "="; }
       | tDIFF {$$ = "!=" ; }
       | tSUPEQ {$$ = ">="; }
       | tINFEQ {$$ = "<="; }
       | tSUP {$$ = ">"; }
       | tINF {$$ = "<"; }
       ;

select_list: tTAGO1 column_ref {add_to_query($2, SPACE);} tTAGC1
           | tTAGO2 column_ref {
                                     char c=',';
                                     strncat($2,&c,1);
                                     add_to_query($2, NOSPACE);
                                } tSEP select_list tTAGC2 ;

column_name: tID {$$=$1;} ;

column_ref: column_location {$$=$1;}  
          | set_function {$$=$1;} 
          | renamed_set_function {$$ = $1;} ;

renamed_set_function: tAS column_location tSEP set_function tTAGC1 {
                                                                char * as = "AS\0";
                                                                 $$ = concat3($4,as,$2,SPACE) ;
                                                               }

set_function: set_function_type tTAGO3 column_name tTAGC3 tTAGC1 {
                                                                    char * p = ")\0";
                                                                    $$ = concat3($1,$3,p,NOSPACE);
                                                                  } ;
            | set_quantifier tTAGO3 column_name tTAGC3 tTAGC1 {
                                                                    char * p = ")\0";
                                                                    $$ = concat3($1,$3,p,NOSPACE);
                                                                  } ;
            | set_function_type set_quantifier tTAGO3 column_name tTAGC3 tTAGC1 tTAGC1 {
                                                                                 char * end = "))\0";
                                                                                 strncat($4,end,2);
                                                                                 $$ = concat3($1,$2,$4,NOSPACE);
                                                                                } ;

set_function_type: tAVG {$$="AVG(";}
                 | tMIN {$$="MIN(";}
                 | tMAX {$$="MAX(";}
                 | tCOUNT {$$="COUNT(";}
                 | tSUM {$$="SUM(";}
                 ;

set_quantifier: tDISTINCT {$$="DISTINCT(";} ;

column_location: column_name {$$=$1;}
          | table_ref tDOT column_name {
                                          char * d = ".\0";
                                          $$ = concat3($1,d,$3,NOSPACE);
          };

table_ref: tID {$$=$1;} ;

table_ref_in_group: tID {
                            char c=',';
                            strncat($1,&c,1);
                            add_to_query($1, NOSPACE);
                         }; 

tTAGO2: tINF;
tTAGC2: tSUP;

%%

void yyerror(char* s) {
	fprintf(stderr, "%s, at %d, look ahead : %d\n", s, yylineno, yychar);
	//exit(-1);
}

int main() {
	init();
	yyparse();
	ins_print();
  free_tree();
}