import gramfuzz
from gramfuzz.fields import *

class NRef(Ref):
    cat = "query_def"
class NDef(Def):
    cat = "query_def"


Def("query",
    NRef("ordered_or_select_query"), cat="query", sep=" ")
NDef("select_query", Or("SELECT", "SELECT_ALL"), sep=" ")

NDef("ordered_or_select_query",Or(NRef("select_query"), And("ORDER_BY[", NRef("ordered_select_query"), NRef("tagc1"), sep=" ")))
NDef("ordered_select_query",NRef("sort_list"),NRef("sep"),NRef("select_query"))
NDef("sort_list",Or(And(NRef("tago2"),NRef("sort_spec"),NRef("sep"),NRef("sort_list"),NRef("tagc2")),NRef("sort_list_end")))
NDef("sort_list_end",NRef("tago1"),NRef("sort_spec"),NRef("tagc1"))
#NDef("sort_list",Or(And(NRef("tago2"),NRef("sort_spec"),NRef("sep"),NRef("sort_list")),And(NRef("tago1"),NRef("sort_spec"),NRef("tagc1"))))

NDef("sort_spec","sort_speccc")

NDef("tago1", "[]", sep=" ")
NDef("tagc1", "]", sep=" ")
NDef("tago2", NRef("inf"), sep=" ")
NDef("tagc2", NRef("sup"), sep=" ")
NDef("tago3", "(", sep=" ")
NDef("tagc3", ")", sep=" ")
NDef("inf", "<", sep=" ")
NDef("sup", ">", sep=" ")
NDef("sep",",",sep=" ")