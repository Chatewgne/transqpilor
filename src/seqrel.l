%{
    #include "y.tab.h"
%}

digit	[0-9]
letter	[a-zA-Z]

%%

"SELECT"	{ return tSELECT; }
"SELECT_ALL"	{ return tSELECTALL; }
"FROM"	{ return tFROM; }
"WHERE"	{ return tWHERE; }
"ALL_FROM" {return tALL_FROM; }

"AND[" { return tAND; }
"OR[" {return tOR;} 

"=" {return tEQ;}
"!=" {return tDIFF;}
">=" {return tSUPEQ;}
"<=" {return tINFEQ;}
">" {return tSUP;}
"<" {return tINF;}

"]-" { return tTAGC6; }
"-["  { return tTAGO6; }
"{" { return tTAGO5; }
"}"  { return tTAGC5; }
"((" { return tTAGO4; }
"))" { return tTAGC4; }
"["	{ return tTAGO1; }
"]"	{ return tTAGC1; }
"("	{ return tTAGO3; }
")"	{ return tTAGC3; }

"'" {return tAP;}
"." {return tDOT;}
","	{ return tSEP; }
";"	{ return tSEP2; }
"ORDER_BY["  { return tORDERBY; }
"ASC"  { return tASC; }
"DESC"  { return tDESC; }
"UNION{"  { return tUNION; }
"GROUP_BY-[" { return tGROUPBY;}

"IN[" { return tIN;}
"NOT_IN[" { return tNOTIN;}
"IN{" { return tSUBIN;}
"NOT_IN{" { return tSUBNOTIN;}
"AS[" { return tAS;}
"AS{" { return tASREF;}
"DISTINCT[" { return tDISTINCT;}
"AVG[" { return tAVG;}
"MIN[" { return tMIN;}
"MAX[" { return tMAX;}
"COUNT[" { return tCOUNT;}
"SUM[" { return tSUM;}


-?{digit}+	{ 
				yylval.str = strdup(yytext);
				return tNB;  
			}
{letter}({letter}|{digit}|_)*	{ 
									yylval.str = strdup(yytext);
									return tID; 
								}

[ \t\n]+	{ }

. { 
	fprintf(stderr, "incorrect token: '%s'\n",yytext);
	return(1);
	}

