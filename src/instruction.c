#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "../header/instruction.h"

char query[QUERY_SIZE];
int i;

void init(void) {
	query[0]='\0';
    i=0;
}

char * new_buffer(){
    return (char *)malloc(BUFFER_SIZE * sizeof(char));
}

void ins_print(void) {
		printf("%s\n",query);
}

void ins_write() {
    //TODO

    /*	FILE* file = fopen(PATH_INS, "w+"); 

	  for (int i = 0; i < ins_index; i++) {
		fprintf(file, "%5s %d %d %d\n", operation_2_string[ins[i].o], ins[i].a, ins[i].b, ins[i].c);
	  }

	  fclose(file); */
}

void add_to_query(const char *str, int space){
    int length= strlen(str);
    if ((i+length+1) > QUERY_SIZE) {
        perror("Error : Query too big : %d\n");
    }
    else {
        strncpy(&query[i],str,(QUERY_SIZE-i));
        i+=length;
        if (space){
            query[i]=' ';
            i++;
        }
    }
}

char * concat3(const char *str1, const char *str2, const char *str3, int space){
    char * str = (char*)malloc(strlen(str1)+strlen(str2)+strlen(str3)+2);
    char c=' ';
    strncat(str,str1,strlen(str1));
    if (space){
        strncat(str,&c,1);
    }
    strncat(str,str2,strlen(str2));
    if (space){
        strncat(str,&c,1);
    }
    strncat(str,str3,strlen(str3));
    return str;
}

char * concat2(const char *str1, const char *str2, int space){
    char * str = (char*)malloc(strlen(str1)+strlen(str2)+1);
    char c=' ';
    strncat(str,str1,strlen(str1));
    if (space){
        strncat(str,&c,1);
    }
    strncat(str,str2,strlen(str2));
    if (space){
        strncat(str,&c,1);
    }
    return str;
}

void add_3_to_query(const char *str1, const char *str2, const char *str3, int space){
    char * str = concat3(str1,str2,str3,NOSPACE);
    add_to_query(str, space);
}

void concat_buff(const char *str, char * buff, int space){
    int length= strlen(str) + strlen(buff);
    if ((length+1) > BUFFER_SIZE) {
        perror("Error : Buffer is full\n");
    }
    else {
        //printf("Adding %s to buff\n",str);
        strcat(buff,str);
        if (space){
            char c = ' ';
            strncat(buff,&c,1);
        }
    }
}


