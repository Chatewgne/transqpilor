# command
LEX = lex
YACC = yacc
CC = gcc

# option 
LLFLAGS = #-d 
YYFLAGS = -d -v
CCFLAGS = -Wall # -g
LIBS = -ly -ll

# directory
SOURCE = src
BUILD = build
OUTPUT = bin
INPUT = input
HEADER = header

# file
SRC = $(wildcard $(SOURCE)/*.c)
OBJ = $(SRC:$(SOURCE)/%.c=$(BUILD)/%.o)
TARGET = $(OUTPUT)/compiler
TEST_ERROR = $(wildcard $(INPUT)/error/*.c)
TEST_SUCCESS = $(wildcard $(INPUT)/success/*.c)

all: $(TARGET)

$(TARGET): $(BUILD)/y.tab.o $(BUILD)/lex.yy.o $(OBJ)
	$(CC) $(CCFLAGS) $^ -o $@ $(LIBS)

$(BUILD)/y.tab.o: $(BUILD)/y.tab.c
	$(CC) $(CCFLAGS) -c -o $@ $<  

$(BUILD)/lex.yy.o: $(BUILD)/lex.yy.c

$(BUILD)/%.o: $(SOURCE)/%.c $(HEADER)/%.h
	$(CC) $(CCFLAGS) -c -o $@ $<  

$(BUILD)/y.tab.c: $(SOURCE)/rule.y
	$(YACC) $(YYFLAGS) -o $@ $^

$(BUILD)/lex.yy.c: $(SOURCE)/seqrel.l
	$(LEX) $(LLFLAGS) -o $@ $^ 

test: $(TARGET)
	$(TARGET) < $(INPUT)/input_file
	
test_success: $(TARGET)
	for file in $(TEST_SUCCESS) ; do \
		echo "current file :" $$file; \
		$(TARGET) < $$file; \
	done

test_error: $(TARGET)
	for file in $(TEST_ERROR) ; do \
		echo "current file :" $$file; \
		$(TARGET) < $$file; \
	done

debug_lex: 
	$(eval LLFLAGS = $(LLFLAGS) -d)

debug_yacc: 
	$(eval YYFLAGS = $(YYFLAGS) -t)

debug_st_fun:
	$(eval CCFLAGS = $(CCFLAGS) -DDEBUG_ST_FUN)

debug_st:
	$(eval CCFLAGS = $(CCFLAGS) -DDEBUG_ST)

clean:
	rm $(BUILD)/* $(OUTPUT)/*

