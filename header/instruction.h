#define QUERY_SIZE 50000
#define SPACE 1
#define NOSPACE 0
#define BUFFER_SIZE 500

void init(void);
void ins_print(void);
void ins_write();
void add_to_query(const char *str, int space);
char * concat3(const char *str1, const char *str2, const char *str3, int space);
char * concat2(const char *str1, const char *str2, int space);
void add_3_to_query(const char *str1, const char *str2, const char *str3, int space);
char * new_buffer();
void concat_buff(const char *str, char * buff, int space);
